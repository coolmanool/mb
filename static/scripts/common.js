var csrfSafeMethod = function(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};

var ajaxSetup = function(csrftoken) {
  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader('X-CSRFToken', csrftoken);
          }
      }
  });
};

var setError = function(text){
  var $errorElem = $('#error-auth');
  $errorElem.css({'visibility': 'visible'});
  $errorElem.html(text);
};

var setPage = function(dist, link){
  var $btn = $('#page-'+ dist);
  if (link === null){
    $btn.addClass('disabled');
    $btn.attr({'tab-index': '-1'});
    $btn.attr({'data-href': null});
  } else {
    $btn.removeClass('disabled');
    $btn.removeAttr('tab-index');
    $btn.attr({'data-href': link});
  }
};

var pageOnClick = function(){
  $this = $(this);
  api_url = $this.attr('data-href');
  if (!api_url) {
    return;
  }
  loadBooks(api_url);
};
