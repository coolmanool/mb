var loadRecomendBooks = function(){
  $.ajax({
    url: '/api/bookrecomendlist/',
    method: 'GET',
    headers: {'accept': 'application/json; version=5', 'content-type': 'application/json'},
    dataType: 'json',
    success: function(data){
      var $bl = $('#booklist .col');
      $bl.empty();
      if (data.data.books.length == 0){
        $bl.append('<h5>Для вас рекомендаций нет. Удалите какие-нибудь книги из вашего списка и возвращайтесь :)</h5>');
        return;
      }
      $.each(data.data.books, function(index, book){
        $bl.append(generateBookBlock(book));
      });
      $('.btn-add-book').on('click', addBook);
      $('.btn-del-book').on('click', deleteBook);
    }
  });
};

$(document).ready(function(){
  loadRecomendBooks();
})
