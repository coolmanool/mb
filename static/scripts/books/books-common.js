var generateBookBlock = function(book){
  return '<div class="row wrapper wrapper-book wrapper-blue"><div class="col-2">'
    + '<img src="'
    + book.image_url
    + '"></div><div class="col-8"><h5>'
    + book.name
    + '</h5><h6>'
    + book.authors
    + '</h6></div><div class="col-2">'
    + '<button class="btn btn-sm btn-success btn-add-book" data-id="'
    + book.id
    + '">Добавить</button><button class="btn btn-sm btn-danger btn-del-book" data-id="'
    + book.id
    + '">Удалить</button></div></div>';
};

var goToMyBooks = function(){
  $(location).attr('href', '/');
};

var goToRecomendBooks = function(){
  $(location).attr('href', '/books');
};

var addBook = function(){
  addDelBook($(this), 'add');
};

var deleteBook = function(){
  addDelBook($(this), 'delete');
};

var addDelBook = function($this, method){
  var book_id = $this.attr('data-id');
  var csrf_token = $('[name=csrfmiddlewaretoken]').val();
  ajaxSetup(csrf_token);
  $.ajax({
    url: '/api/' + method + 'book/',
    method: 'POST',
    headers: {'accept': 'application/json; version=5', 'content-type': 'application/json'},
    data: JSON.stringify({
      'csrfmiddlewaretoken': csrf_token,
      'book_id': book_id
    }),
    dataType: 'json',
    success: function(){
      $('button[data-id=' + book_id + ']').removeClass('hide').addClass('show');
      $this.removeClass('show').addClass('hide');
    }
  });
};

$(document).ready(function(){
  $('#btn-books-my').on('click', goToMyBooks);
  $('#btn-books-recomend').on('click', goToRecomendBooks);
})
