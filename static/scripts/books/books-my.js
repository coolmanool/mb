var setPages = function(meta) {
  setPage('prev', meta.prev);
  setPage('next', meta.next);
};

var loadBooks = function(url){
  $.ajax({
    url: url,
    method: 'GET',
    headers: {'accept': 'application/json; version=5', 'content-type': 'application/json'},
    dataType: 'json',
    success: function(data){
      var $bl = $('#booklist .col');
      $bl.empty();
      if (data.data.books.length == 0){
        $bl.append('<h5>Кажется, у вас пока нет книг. Загляните в раздел рекомендованное :)</h5>');
        return;
      }
      $.each(data.data.books, function(index, book){
        $bl.append(generateBookBlock(book));
      });
      $('.btn-add-book').on('click', addBook);
      $('.btn-del-book').on('click', deleteBook);
      setPages(data.data.meta);
    }
  });
};

$(document).ready(function(){
  loadBooks('/api/bookuserlist/?cursor=0');
  $('.page-item').on('click', pageOnClick);
})
