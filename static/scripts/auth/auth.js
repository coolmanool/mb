var auth = function(){
  var $form = $('#form-auth')[0];

  if ($form.login.value == '' || $form.password.value == '') {
    setError('Пожалуйста, заполните все поля.');
    return;
  }

  ajaxSetup($form.csrfmiddlewaretoken.value);
  $.ajax({
    url: '/api/auth/',
    method: 'POST',
    headers: {'accept': 'application/json; version=5', 'content-type': 'application/json'},
    data: JSON.stringify({
      'csrfmiddlewaretoken': $form.csrfmiddlewaretoken.value,
      'login': $form.login.value,
      'password': $form.password.value
    }),
    dataType: 'json',
    success: function(){
      location.reload();
    },
    error: function(data) {
      setError(data.responseJSON.data.error);
    }
  })
};

$(document).ready(function() {
  $('#button-signin').on('click', auth);
})
