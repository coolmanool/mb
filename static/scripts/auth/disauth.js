var disauth = function() {
  var csrf_token = $('[name=csrfmiddlewaretoken]').val();
  ajaxSetup(csrf_token);
  $.ajax({
    url: '/api/disauth/',
    method: 'POST',
    headers: {'accept': 'application/json; version=5', 'content-type': 'application/json'},
    data: JSON.stringify({
      'csrfmiddlewaretoken': csrf_token
    }),
    dataType: 'json',
    complete: function(){
      $(location).attr('href', '/');
    }
  });
};

$(document).ready(function(){
  $('#btn-disauth').on('click', disauth);
})
