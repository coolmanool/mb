var auth = function(){
  var $form = $('#form-auth')[0];

  if ($form.email.value == '' || $form.password.value == '') {
    setError('Пожалуйста, заполните все поля.');
    return;
  }

  ajaxSetup($form.csrfmiddlewaretoken.value);
  $.ajax({
    url: '/apimybook/auth/',
    method: 'POST',
    headers: {'content-type': 'application/json'},
    data: JSON.stringify({
      'csrfmiddlewaretoken': $form.csrfmiddlewaretoken.value,
      'email': $form.email.value,
      'password': $form.password.value
    }),
    dataType: 'json',
    success: function(){
      location.reload();
    },
    error: function(data) {
      setError(data.responseJSON.data.error);
    }
  })
};

$(document).ready(function() {
  $('#button-signin').on('click', auth);
})
