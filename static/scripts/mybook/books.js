var generateBookBlock = function(book){
  return '<div class="row wrapper wrapper-book wrapper-blue"><div class="col-2">'
    + '<img src="'
    + book.image_url
    + '"></div><div class="col-8"><h5>'
    + book.name
    + '</h5><h6>'
    + book.authors
    + '</h6></div></div>';
};

var setPages = function(meta) {
  setPage('prev', meta.previous);
  setPage('next', meta.next);
};

var loadBooks = function(api_url=''){
  $.ajax({
    url: '/apimybook/books/',
    method: 'GET',
    data: {'api_url': api_url},
    headers: {'content-type': 'application/json'},
    dataType: 'json',
    success: function(data){
      var $bl = $('#booklist .col');
      $bl.empty();
      if (data.data.books.length == 0){
        $bl.append('<h5>Кажется, у вас пока нет книг.</h5>');
        return;
      }
      $.each(data.data.books, function(index, book){
        $bl.append(generateBookBlock(book));
      });
      setPages(data.data.meta);
    },
    error: function(data){
      var $bl = $('#booklist .col');
      $bl.empty();
      $bl.append('<h5>Кажется, произошла ошибка!</h5>');
    }
  });
};

$(document).ready(function(){
  loadBooks();
  $('.page-item').on('click', pageOnClick);
})
