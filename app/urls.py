from django.urls import path

from . import views

urlpatterns = [
    path('', views.index_old, name='index_old'),
    path('books/', views.books, name='books'),

    path('mybook/', views.index, name='index')
]
