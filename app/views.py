from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.decorators.http import require_GET

from api.forms import AuthForm

@require_GET
def index_old(request):
    if request.user.is_authenticated:
        return render(request, 'books-my.html')
    else:
        return render(request, 'auth.html', {'form': AuthForm()})


@require_GET
def index(request):
    session = request.COOKIES.get('session_mybook', None)
    if session is not None:
        return render(request, 'mb-books.html')
    else:
        return render(request, 'mb-auth.html')


@login_required
@require_GET
def books(request):
    return render(request, 'books-recomend.html')
