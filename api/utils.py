import re
from .config import Config
from django.http import JsonResponse


def api_decor(func):
    def wrapper(request):
        if not request.is_ajax():
            return ResponseUtil.error('Кажется, вы используете неправильный запрос.')

        v = re.search(r'; version=(?P<version>\d+)', request.headers['Accept'])
        if v is not None and int(v.group('version')) == Config.VERSION:
            return func(request)

        return ResponseUtil.error('Вы используете устаревшую версию или неправильный api. Текущая версия - 5.')
    return wrapper


class ResponseUtil(object):
    @classmethod
    def success(cls, data=None, cookies=None):
        return cls.__base_response(data, cookies=cookies)

    @classmethod
    def bad_request(cls):
        return cls.error('Неверные параметры запроса.', status=400)

    @classmethod
    def error(cls, error_msg='Непредвиденная ошибка.', status=404):
        return cls.__base_response({'error': error_msg}, status=status)

    @classmethod
    def __base_response(cls, data, status=200, cookies=None):
        r = JsonResponse(data={'data': data}, status=status)

        if cookies is None:
            return r

        for k, v in cookies.items():
            r.set_cookie(k, v)

        return r
