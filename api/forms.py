from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class AddDelBookForm(forms.Form):
    book_id = forms.IntegerField(min_value=1)


class CursorForm(forms.Form):
    cursor = forms.IntegerField(min_value=0)


class AuthForm(forms.Form):
    login = forms.CharField(
        label='login',
        max_length=150,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ник или e-mail'})
    )
    password = forms.CharField(
        label='password',
        max_length=150,
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'})
    )

    def clean(self):
        cleaned_data = super().clean()
        login = cleaned_data.get('login')

        if '@' not in login:
            return cleaned_data

        try:
            user = User.objects.get(email=login)
        except ObjectDoesNotExist:
            raise forms.ValidationError('User with this email does not exits')

        cleaned_data['login'] = user.username

        return cleaned_data
