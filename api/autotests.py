from django.test import Client, TestCase

class AutoTest(TestCase):
    @classmethod
    def setUpClass(self):
        self.client = Client()

    def test_test(self):
        response = self.client.get('/api/test/')
        self.assertEqual(response.status_code, 200)
