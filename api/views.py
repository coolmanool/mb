import json
from .config import Config
from .forms import AddDelBookForm, AuthForm, CursorForm
from .models import Author, Book
from .utils import api_decor, ResponseUtil
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.http import require_GET, require_POST


class BooksView(View):
    @method_decorator(api_decor)
    @method_decorator(login_required)
    def get(self, request):
        f = CursorForm(request.GET)
        if not f.is_valid():
            return ResponseUtil.bad_request()

        cursor = f.cleaned_data['cursor']
        user = User.objects.get(username=request.user)
        books = user.books.all().order_by('name').prefetch_related('authors')[cursor:cursor + Config.BOOKS_VIEW_OFFSET]
        count = user.books.count()
        return ResponseUtil.success(
            {
                'books': self.get_books_list(books),
                'meta': {
                    'prev': self.__get_prev_offset(cursor),
                    'next': self.__get_next_offset(cursor, count)
                }
            }
        )

    @classmethod
    def get_books_list(cls, books):
        return [
            {
                'id': b.id,
                'name': b.name,
                'image_url': b.image.url if b.image else Config.DEFAULT_IMAGE_URL,
                'authors': ', '.join([a.name for a in b.authors.all()])
            } for b in books
        ]

    def __get_prev_offset(self, cursor):
        if cursor < Config.BOOKS_VIEW_OFFSET:
            return None

        return '{}{}'.format(Config.BOOKS_CURSOR_URL, cursor - Config.BOOKS_VIEW_OFFSET)

    def __get_next_offset(self, cursor, count):
        if cursor + Config.BOOKS_VIEW_OFFSET >= count:
            return None

        return '{}{}'.format(Config.BOOKS_CURSOR_URL, cursor + Config.BOOKS_VIEW_OFFSET)


@api_decor
@require_POST
def auth(request):
    data = json.loads(request.body.decode('utf-8'))
    auth_form = AuthForm(data)

    if not auth_form.is_valid():
        return ResponseUtil.error('Юзера с таким емейлом не существует или вы пытаетесь отправить на сервер плохое содержимое.')

    data = auth_form.cleaned_data
    user = authenticate(username=data['login'], password=data['password'])

    if user is None:
        return ResponseUtil.error('Такого юзера не существует. Проверьте ваши идентификационные данные.')

    login(request, user)

    return ResponseUtil.success()


@api_decor
@require_POST
@login_required
def disauth(request):
    logout(request)
    return ResponseUtil.success()


@api_decor
@require_POST
@login_required
def delete_book(request):
    data = json.loads(request.body.decode('utf-8'))
    f = AddDelBookForm(data)
    if not f.is_valid():
        print(f)
        return ResponseUtil.bad_request()

    book = Book.objects.get(id=f.cleaned_data['book_id'])
    user = User.objects.get(username=request.user)
    user.books.remove(book)
    return ResponseUtil.success()


@api_decor
@require_POST
@login_required
def add_book(request):
    data = json.loads(request.body.decode('utf-8'))
    f = AddDelBookForm(data)
    if not f.is_valid():
        return ResponseUtil.bad_request()

    book = Book.objects.get(id=f.cleaned_data['book_id'])
    user = User.objects.get(username=request.user)
    user.books.add(book)
    return ResponseUtil.success()


@api_decor
@require_GET
@login_required
def get_recomend_books(request):
    user = User.objects.get(username=request.user)
    user_books = set(user.books.all())
    books = set(Book.objects.all().prefetch_related('authors'))
    recomend_books = list(books - user_books)[0:5]
    return ResponseUtil.success({'books': BooksView.get_books_list(recomend_books)})


@require_GET
@login_required
def test(request):
    return ResponseUtil.success()
