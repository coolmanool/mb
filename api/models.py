from django.contrib.auth.models import User
from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=255)

class Book(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(null=True, blank=True)
    authors = models.ManyToManyField(Author, related_name='books')
    users = models.ManyToManyField(User, related_name='books')
