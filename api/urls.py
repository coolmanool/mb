from django.urls import path

from . import views

urlpatterns = [
    path('bookuserlist/', views.BooksView.as_view(), name='books'),
    path('bookrecomendlist/', views.get_recomend_books, name='get_recomend_books'),

    path('addbook/', views.add_book, name='add_book'),
    path('deletebook/', views.delete_book, name='delete_book'),

    path('auth/', views.auth, name='auth'),
    path('disauth/', views.disauth, name='disauth'),

    path('test/', views.test, name='test')
]
