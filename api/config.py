class Config(object):
    VERSION = 5

    BOOKS_CURSOR_URL = '/api/bookuserlist/?cursor='
    BOOKS_VIEW_OFFSET = 5

    DEFAULT_IMAGE_URL = '/media/noimage.png'
