class Config(object):
    MYBOOK_API_URL = 'https://mybook.ru'
    MYBOOK_IMAGES_URL = 'https://i4.mybook.io/c/256x426/'
    DEFAULT_IMAGE_URL = '/media/noimage.png'
