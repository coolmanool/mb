from django.urls import path

from . import views

urlpatterns = [
    path('books/', views.BooksView.as_view(), name='books'),
    path('auth/', views.auth, name='auth'),

    # path('test/', views.test, name='test')
]
