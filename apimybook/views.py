# coding: utf-8

import json
import requests
from .config import Config
from .utils import Utils
from api.utils import ResponseUtil
from django.views import View
from django.views.decorators.http import require_GET, require_POST


class BooksView(View):
    def get(self, request):
        session = request.COOKIES.get('session_mybook', None)

        if session is None:
            return ResponseUtil.error()

        query = request.GET.get('api_url', '')
        if query == '':
            query = '/api/bookuserlist/?limit=10'

        r = requests.get(
            '{}{}'.format(Config.MYBOOK_API_URL, query),
            headers={'accept': 'application/json; version=5'},
            cookies={'session': session}
        )

        r_json = r.json()

        return ResponseUtil.success({
            'books': [
                {
                    'name': obj['book']['name'],
                    'authors': obj['book']['authors_names'],
                    'image_url': self.__get_image_url(obj['book']['default_cover']),
                } for obj in r_json['objects']
            ],
            'meta': r_json['meta']
        })

    def __get_image_url(self, image_path):
        if image_path is None or image_path == '':
            return Config.DEFAULT_IMAGE_URL

        return '{}{}'.format(Config.MYBOOK_IMAGES_URL, image_path)

@require_POST
def auth(request):
    data = json.loads(request.body.decode('utf-8'))

    r = requests.post(
        '{}/api/auth/'.format(Config.MYBOOK_API_URL),
        json={'email': data['email'], 'password': data['password']}
    )

    if r.status_code == 400:
        return ResponseUtil.error('Неправильный e-mail или пароль.')
    elif r.status_code != 200:
        return ResponseUtil.error()
    return ResponseUtil.success(cookies={'session_mybook': r.cookies['session']})


def test(request):
    return ResponseUtil.success()
