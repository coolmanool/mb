from django.apps import AppConfig


class ApimybookConfig(AppConfig):
    name = 'apimybook'
